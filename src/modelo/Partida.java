package modelo;

public class Partida {
	private int id;
	private String local;
	private String adv;
	private String juiz;
	private String time;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public String getAdv() {
		return adv;
	}
	public void setAdv(String adv) {
		this.adv = adv;
	}
	public String getJuiz() {
		return juiz;
	}
	public void setJuiz(String juiz) {
		this.juiz = juiz;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}

}
