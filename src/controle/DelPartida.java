package controle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Partida;

/**
 * Servlet implementation class DelPartida
 */
@WebServlet("/DelPartida")
public class DelPartida extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DelPartida() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Partida par = new Partida();
		par.setTime(request.getParameter("time"));
		if(new ControlePartida().deletPartida(par)) {
			request.getRequestDispatcher("CadastroJogo.jsp").forward(request, response);
		}else {
			request.getRequestDispatcher("404.jsp").forward(request, response);
		}

	}

}
