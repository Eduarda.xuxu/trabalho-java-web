package controle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Partida;

/**
 * Servlet implementation class ServPartida
 */
@WebServlet("/ServPartida")
public class ServPartida extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServPartida() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Partida partida = new Partida();
		partida.setLocal(request.getParameter("local"));
		partida.setAdv(request.getParameter("adv"));
		partida.setTime(request.getParameter("time"));
		partida.setJuiz(request.getParameter("juiz"));
		if(new ControlePartida().insertPartida(partida)) {
			request.getRequestDispatcher("CadastroJogo.jsp").forward(request, response);
		}else {
			request.getRequestDispatcher("404.jsp").forward(request, response);
		}
	}

}
