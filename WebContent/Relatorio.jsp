<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Relat�rio Final do Jogo</title>
<meta charset='UTF-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
	
	<!-- Site Properties -->
	<link rel='stylesheet' type='text/css' href='Visual/css/semantic.css'>
	<link rel='stylesheet' type='text/css' href='Visual/css/custom.css'>
	<link rel='stylesheet' type='text/css' href='Visual/components/icon.css'>

</head>
<body>
	<div class='ui large top fixed hidden menu'>
		<div class='ui container'>
			<a class='active item' href='index.jsp'>Home</a>
			<div class='right menu'>
				<a class='item' href='login.php'>Login</a>
				<a class='item' href='cadastro.jsp'>Cadastre-se</a>
				<a class='item' href='Relatorio.jsp'>Rel�torio</a>
			</div>
		</div>
	</div>

	<!-- menu mobile -->
	<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
		<a class='active item' href='index.jsp'>Home</a>
		<a class='item' href='login.jsp'>Login</a>
		<a class='item' href='cadastro.jsp'>Cadastrar</a>
		<a class='item' href='Relatorio.jsp'>Rel�torio</a>
	</div>
	
	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item' href='index.jsp'>Home</a>
					<div class='right menu'>
						<a class='item' href='login.jsp'>Login</a>
						<a class='item' href='cadastro.jsp'>Cadastrar</a>
						<a class='item' href='Relatorio.jsp'>Rel�torio</a>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class='ui main text container'>
    		<h1 class='ui header'>Relat�rio Final do Jogo</h1>
    		<p>Aqui vai ser escrito o relat�rio com o conte�do que for cadastrado no sistema pelo Apoio e o Auxiliar T�cnico.</p>
    		
    	</div>
	</div> <!-- fim menu pusher -->
  		
  	<!-- footer -->
	<div class='ui inverted vertical footer segment ' style='background-color: #080b34;'>
		<div class='ui container'>
			<div class='ui stackable inverted divided equal height stackable grid'>
				<div class='three wide column'>
					<h4 class='ui inverted header'>Altores</h4>
					<div class='ui inverted link list'>
						<a  class='item'>Marcela Eduarda</a>
						<a  class='item'>Kailanne</a>
						<a  class='item'>Kau� Duarte</a>		
					</div>
				</div>
				<div class='three wide column'>
					<h4 class='ui inverted header'>Redes Socias</h4>
					<div class='ui inverted link list'>
						<a href='#' class='item'><i class='facebook icon'></i> Facebook</a>
						<a href='#' class='item'><i class='instagram icon'></i> Instagram</a>
						<a href='#' class='item'><i class='twitter icon'></i> Twitter</a>
					</div>
				</div>
				<div class='seven wide column'>
					<img src='img/icon.png' alt='Imagem da logo' class='ui small image'>
					<h4 class='ui inverted header'>ReportSport</h4>		
				</div>
			</div>
		</div>
	</div>
			
</body>
<script src='Visual/js/jquery.min.js'></script>
<script src='Visual/js/semantic.js'></script>
<script src='Visual/components/visibility.js'></script>
<script src='Visual/components/sidebar.js'></script>
<script src='Visual/components/transition.js'></script>
<script src='Visual/components/modal.min.js'></script>	
<script src='Visual/js/teste.js'></script>
</html>