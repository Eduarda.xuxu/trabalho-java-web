<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Insert title here</title>
	<meta charset="UTF-8">
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>

    <link rel='stylesheet' type='text/css' href='Visual/css/semantic.css'>
	<link rel='stylesheet' type='text/css' href='Visual/css/custom.css'>
	<link rel='stylesheet' type='text/css' href='Visual/components/icon.css'>
	<link rel='stylesheet' type='text/css' href='Visual/css/visual.css'>
    <link href="Visual/imagens">
    <title>Cadastro Jogo</title>
</head>
<body class='body'>
	<div class='ui large top fixed hidden menu'>
		<div class='ui container'>
			<a class='active item' href='index.jsp'>Home</a>
			<div class='right menu'>
				<a class='item' href='login.jsp'>Login</a>
				<a class='item' href='cadastro.jsp'>Cadastre-se</a>
				<a class='item' href='Relatorio.jsp'>Rel�torio</a>
			</div>
		</div>
	</div>
	<!-- menu mobile -->
	<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
		<a class='active item' href='index.jsp'>Home</a>
		<a class='item' href='login.jsp'>Login</a>
		<a class='item' href='cadastro.jsp'>Cadastrar</a>
		<a class='item' href='Relatorio.jsp'>Rel�torio</a>
	</div>
	
	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item' href='index.jsp'>Home</a>
					<div class='right menu'>
						<a class='item' href='login.jsp'>Login</a>
						<a class='item' href='cadastro.jsp'>Cadastrar</a>
						<a class='item' href='Relatorio.jsp'>Rel�torio</a>
					</div>
				</div>
			</div>
		</div>
		<br /><br />
		<div class='ui container'>
		  <div class='ui middle aligned center aligned grid'>
			<div class='column'>
				<h1 style='color: white;text-shadow: 0.2em 0.2em 0.3em black;'>
					Informa��es do Jogo e Classifica��o dos Pontos
				</h1>		
				<form class='ui large form' action='ServPartida' method='post'>
					<div class='ui stacked segment'>
						<div class='for'>
							<div>
							    <label>Vencedor do Jogo:</label>
							    <input type='text' name='first-name' >
							</div>
							<br />
							<div>
							    <label>Quantidade de Pontos do Time:</label>
							    <input type='text' name='first-name' >
							</div>
							<br />
							<div>
							    <label>Quantidade de Pontos do Advers�rio:</label>
							    <input type='text' name='first-name' >
							</div>							    
						</div>
					
					  <br />
					<input type='submit' class='ui large submit button' style='background-color: #080b34;color: white;' value='Cadastrar Informa��es' />
					</div>
				</form>
				</div>
			</div>
		</div>
		</div> <!-- fim menu pusher -->
</body>
<script src='Visual/js/jquery.min.js'></script>
<script src='Visual/js/semantic.js'></script>
<script src='Visual/components/visibility.js'></script>
<script src='Visual/components/sidebar.js'></script>
<script src='Visual/components/transition.js'></script>
<script src='Visual/components/modal.min.js'></script>	
<script src='Visual/js/teste.js'></script>
</html>