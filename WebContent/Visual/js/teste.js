$(document)
  .ready(function () {
    // menu fixo quando aprovado
    $('.masthead')
      .visibility({
        once: false,
        onBottomPassed: function () {
          $('.fixed.menu').transition('fade in');
        },
        onBottomPassedReverse: function () {
          $('.fixed.menu').transition('fade out');
        }
      })

    // criar barra lateral e anexar ao menu aberto
    $('.ui.sidebar')
      .sidebar('attach events', '.toc.item')

  });

$(document)
  .ready(function () {
    $('.ui.form')
      .form({
        fields: {
          email: {
            identifier: 'email',
            rules: [
              {
                type: 'empty',
                prompt: 'Email obrigatorio'
              },
              {
                type: 'email',
                prompt: 'Por favor o email é obrigatoria'
              }
            ]
          },
          password: {
            identifier: 'password',
            rules: [
              {
                type: 'empty',
                prompt: 'Senha obrigatoria'
              },
              {
                type: 'length[6]',
                prompt: 'A enha deve ter pelo menos 6 caracteres'
              }
            ]
          }
        }
      })
      ;
  });
var password = document.getElementById("senha")
  , confirm_password = document.getElementById("confsenha");

function validatePassword() {
  if (password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Senhas diferentes!");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

