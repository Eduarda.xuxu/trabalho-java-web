<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Editar Iforma��es</title>
	<meta charset='UTF-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
	
	<!-- Site Properties -->
	<link rel='stylesheet' type='text/css' href='Visual/css/semantic.css'>
	<link rel='stylesheet' type='text/css' href='Visual/css/custom.css'>
	<link rel='stylesheet' type='text/css' href='Visual/components/icon.css'>
	<link rel='stylesheet' type='text/css' href='Visual/css/visual.css'>

</head>
<body>
	<div class='ui large top fixed hidden menu'>
		<div class='ui container'>
			<a class='active item' href='index.jsp'>Home</a>
			<div class='right menu'>
				<a class='item' href='login.jsp'>Login</a>
				<a class='item' href='cadastro.jsp'>Cadastre-se</a>
				<a class='item' href='Relatorio.jsp'>Rel�torio</a>
			</div>
		</div>
	</div>

	<!-- menu mobile -->
	<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
		<a class='active item' href='index.jsp'>Home</a>
		<a class='item' href='login.jsp'>Login</a>
		<a class='item' href='cadastro.jsp'>Cadastrar</a>
		<a class='item' href='Relatorio.jsp'>Rel�torio</a>
	</div>
	
	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item' href='index.jsp'>Home</a>
					<div class='right menu'>
						<a class='item' href='login.jsp'>Login</a>
						<a class='item' href='cadastro.jsp'>Cadastrar</a>
						<a class='item' href='Relatorio.jsp'>Rel�torio</a>
					</div>
				</div>
			</div>
		</div>
		<br /><br />
		
	<div class='ui container'>
		<div class='ui middle aligned center aligned grid'>
			<div class='column'>
				<h1 >
					Editar Informa��es da Partida e dos Jogadores
				</h1>		
				<form class='ui large form' action='ServPartida' method='post'>
					<div class='ui stacked segment'>
						<div class='for'>
						<div class='ui form'>
						  <div class='fields'>
						    <div class='field'>
						      <label>Local:</label>
						      <input type='text' name='local'>
						    </div>
						    <div class='field'>
						      <label>Time Advers�rio:</label>
						      <input type='text' name='adv'>
						    </div>
						    <div class='field'>
						      <label>Nome do juiz:</label>
						      <input type='text' name='juiz'>
						    </div>
						  </div>
						<div class='ui form'>
						  <div class='fields'>
						    <div class='field'>
						      <label>Camisa 1:</label>
						      <input type='text' >
						    </div>
						    <div class='field'>
						      <label>Camisa 2:</label>
						      <input type='text' >
						    </div>
						    <div class='field'>
						      <label>Camisa 3:</label>
						      <input type='text'>
						    </div>
						  </div>
						</div>
						<div class='ui form'>
						  <div class='fields'>
						    <div class='field'>
						      <label>Camisa 4:</label>
						      <input type='text' >
						    </div>
						    <div class='field'>
						      <label>Camisa 5:</label>
						      <input type='text' >
						    </div>
						    <div class='field'>
						      <label>Camisa 6:</label>
						      <input type='text'>
						    </div>
						  </div>
						</div>
						<div class='ui form'>
						  <div class='fields'>
						    <div class='field'>
						      <label>Camisa 7:</label>
						      <input type='text' >
						    </div>
						    <div class='field'>
						      <label>Camisa 8:</label>
						      <input type='text' >
						    </div>
						    <div class='field'>
						      <label>Camisa 9:</label>
						      <input type='text'>
						    </div>
						  </div>
						</div>
						<div class='ui form'>
						  <div class='fields'>
						    <div class='field'>
						      <label>Camisa 10:</label>
						      <input type='text' >
						    </div>
						    <div class='field'>
						      <label>Camisa 11:</label>
						      <input type='text' >
						  	</div>
						  </div>
						 </div>
						
						  <div class='fields'>
						    <div >
						      <label>M�s:</label>
						      <select class='ui search dropdown' name='' id='s'>
					                <option value=''>M�s:</option>
					                <option value='1'>Janeiro</option>
					                <option value='2'>Fevereiro</option>
					                <option value='3'>Mar�o</option>
					                <option value='4'>Abril</option>
					                <option value='5'>Maio</option>
					                <option value='6'>Junho</option>
					                <option value='7'>Julho</option>
					                <option value='8'>Agosto</option>
					                <option value='9'>Setembro</option>
					                <option value='10'>Outubro</option>
					                <option value='11'>Novembro</option>
					                <option value='12'>Dezembro</option>
					              </select>
						    </div>
						    <div class='fields'>
						    <div >  
						      <label>Dia</label>
						      <select class='ui search dropdown' name='' id='s2'>
					                <option value="">1</option>
					                <option value='1'>2</option>
					                <option value='2'>3</option>
					                <option value='3'>4</option>
					                <option value='4'>5</option>
					                <option value='5'>6</option>
					                <option value='6'>7</option>
					                <option value='7'>8</option>
					                <option value='8'>9</option>
					                <option value='9'>10</option>
					                <option value='10'>12</option>
					                <option value='11'>13</option>
					                <option value='12'>14</option>
					                <option value='13'>15</option>
					                <option value='14'>16</option>
					                <option value='15'>17</option>
					                <option value='16'>18</option>
					                <option value='17'>19</option>
					                <option value='18'>20</option>
					                <option value='19'>21</option>
					                <option value='20'>22</option>
					                <option value='21'>23</option>
					                <option value='22'>24</option>
					                <option value='23'>25</option>
					                <option value='24'>26</option>
					                <option value='25'>27</option>
					                <option value='26'>28</option>
					                <option value='27'>29</option>
					                <option value='28'>30</option>
					              </select>
						    </div>
						    </div>
						    <div class='field' id='a'>
						      <label>Ano:</label>
						      <input type='text'>
						    </div>
						  </div>
						</div>
					  <br />
					<input type='submit' class='ui large submit button' style='background-color: #080b34;color: white;' value='Editar Informa��es' />
					</div>
					</div>
				</form>
			</div>
			</div>
		</div>
		<br /><br />
		<div class='ui container'>
		  <div class='ui middle aligned center aligned grid'>
			<div class='column'>
				<h1 >
					Editar Informa��es da Partida e dos Jogadores
				</h1>		
				<form class='ui large form' action='ServPartida' method='post'>
					<div class='ui stacked segment'>
						<div class='for'>
							<div>
							    <label>Vencedor do Jogo:</label>
							    <input type='text' name='first-name' >
							</div>
							<br />
							<div>
							    <label>Quantidade de Pontos do Time:</label>
							    <input type='text' name='first-name' >
							</div>
							<br />
							<div>
							    <label>Quantidade de Pontos do Advers�rio:</label>
							    <input type='text' name='first-name' >
							</div>							    
						</div>
					
					  <br />
					<input type='submit' class='ui large submit button' style='background-color: #080b34;color: white;' value='Editar Informa��es' />
					</div>
				</form>
				</div>
			</div>
		</div>
			
		<br /><br />
			
	</div> <!-- fim menu pusher -->
  		
  	<!-- footer -->
	<div class='ui inverted vertical footer segment ' style='background-color: #080b34;'>
		<div class='ui container'>
			<div class='ui stackable inverted divided equal height stackable grid'>
				<div class='three wide column'>
					<h4 class='ui inverted header'>Altores</h4>
					<div class='ui inverted link list'>
						<a  class='item'>Marcela Eduarda</a>
						<a  class='item'>Kailanne</a>
						<a  class='item'>Kau� Duarte</a>		
					</div>
				</div>
				<div class='three wide column'>
					<h4 class='ui inverted header'>Redes Socias</h4>
					<div class='ui inverted link list'>
						<a href='#' class='item'><i class='facebook icon'></i> Facebook</a>
						<a href='#' class='item'><i class='instagram icon'></i> Instagram</a>
						<a href='#' class='item'><i class='twitter icon'></i> Twitter</a>
					</div>
				</div>
				<div class='seven wide column'>
					<img src='img/icon.png' alt='Imagem da logo' class='ui small image'>
					<h4 class='ui inverted header'>ReportSport</h4>		
				</div>
			</div>
		</div>
	</div>
</body>
</html>